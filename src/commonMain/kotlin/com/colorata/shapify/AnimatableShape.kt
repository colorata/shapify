package com.colorata.shapify

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Shape

abstract class AnimatableShape : Shape {
    var fractionToCircle by mutableStateOf(0f)
    var rotation by mutableStateOf(0f.degrees)

    abstract fun copy(
        fractionToCircle: Float = this.fractionToCircle,
        rotation: Angle = this.rotation
    ): AnimatableShape
}
