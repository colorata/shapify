package com.colorata.shapify

internal infix fun ClosedFloatingPointRange<Float>.fraction(fraction: Float): Float {
    return start + (endInclusive - start) * fraction
}
