package com.colorata.shapify

fun ScallopShape(
    waveHeight: Float = 0.05f,
    periodMultiplier: Float = 10f
) = SinusoidalShape(waveHeight, periodMultiplier)