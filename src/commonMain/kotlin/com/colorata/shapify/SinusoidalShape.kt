@file:Suppress("UnusedReceiverParameter")

package com.colorata.shapify

import androidx.compose.runtime.Stable
import kotlin.math.cos

@Stable
class SinusoidalShape(
    private val waveHeight: Float = 0.05f,
    private val periodMultiplier: Float = 10f
) : PolarBasedShape() {

    private val waveOffset: Float = 0.95f

    override val polarFunction: (theta: Float) -> Float? = { theta ->
        waveOffset +
                (1f - waveOffset) * fractionToCircle +
                waveHeight * (1f - fractionToCircle) *
                cos(periodMultiplier * (theta + rotation.radians))
    }

    override fun copy(fractionToCircle: Float, rotation: Angle): SinusoidalShape {
        return SinusoidalShape(waveHeight, periodMultiplier).apply {
            this.fractionToCircle = fractionToCircle
            this.rotation = rotation
        }
    }

    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass) return false

        other as SinusoidalShape

        return waveHeight == other.waveHeight &&
                periodMultiplier == other.periodMultiplier &&
                fractionToCircle == other.fractionToCircle &&
                rotation == other.rotation
    }

    override fun hashCode(): Int {
        var result = waveOffset.hashCode()
        result = 31 * result + periodMultiplier.hashCode()
        result = 31 * result + waveHeight.hashCode()
        result = 31 * result + polarFunction.hashCode()
        return result
    }
}
