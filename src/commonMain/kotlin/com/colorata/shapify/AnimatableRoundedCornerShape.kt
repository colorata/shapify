package com.colorata.shapify

import androidx.compose.foundation.shape.CornerSize
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

class AnimatableRoundedCornerShape(
    private val topStart: CornerSize,
    private val topEnd: CornerSize,
    private val bottomStart: CornerSize,
    private val bottomEnd: CornerSize
) : AnimatableShape() {
    override fun copy(fractionToCircle: Float, rotation: Angle): AnimatableRoundedCornerShape {
        return AnimatableRoundedCornerShape(topStart, topEnd, bottomStart, bottomEnd).apply {
            this.fractionToCircle = fractionToCircle
            this.rotation = rotation
        }
    }

    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        val minSide = minOf(size.width, size.height)
        val animatedWidthDiff = (size.width - minSide) * fractionToCircle
        val animatedHeightDiff = (size.height - minSide) * fractionToCircle

        val topLeft = if (layoutDirection == LayoutDirection.Ltr) topStart else topEnd
        val topRight = if (layoutDirection == LayoutDirection.Ltr) topEnd else topStart
        val bottomLeft = if (layoutDirection == LayoutDirection.Ltr) bottomStart else bottomEnd
        val bottomRight = if (layoutDirection == LayoutDirection.Ltr) bottomEnd else bottomStart

        val topLeftAnim = topLeft.toPx(size, density)..(minSide / 2) fraction fractionToCircle
        val topRightAnim = topRight.toPx(size, density)..(minSide / 2) fraction fractionToCircle
        val bottomLeftAnim = bottomLeft.toPx(size, density)..(minSide / 2) fraction fractionToCircle
        val bottomRightAnim =
            bottomRight.toPx(size, density)..(minSide / 2) fraction fractionToCircle

        return Outline.Rounded(
            RoundRect(
                top = animatedHeightDiff / 2,
                left = animatedWidthDiff / 2,
                right = size.width - animatedWidthDiff / 2,
                bottom = size.height - animatedHeightDiff / 2,
                topLeftCornerRadius = CornerRadius(topLeftAnim),
                topRightCornerRadius = CornerRadius(topRightAnim),
                bottomLeftCornerRadius = CornerRadius(bottomLeftAnim),
                bottomRightCornerRadius = CornerRadius(bottomRightAnim),
            )
        )
    }

    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass) return false

        other as AnimatableRoundedCornerShape

        return topStart == other.topStart &&
                topEnd == other.topEnd &&
                bottomStart == other.bottomStart &&
                bottomEnd == other.bottomEnd &&
                fractionToCircle == other.fractionToCircle &&
                rotation == other.rotation
    }

    override fun hashCode(): Int {
        var result = topStart.hashCode()
        result = 31 * result + topEnd.hashCode()
        result = 31 * result + bottomStart.hashCode()
        result = 31 * result + bottomEnd.hashCode()
        return result
    }
}

/**
 * Creates [AnimatableRoundedCornerShape] with the same size applied for all four corners.
 * @param corner [CornerSize] to apply.
 */
fun AnimatableRoundedCornerShape(corner: CornerSize) =
    AnimatableRoundedCornerShape(corner, corner, corner, corner)

/**
 * Creates [AnimatableRoundedCornerShape] with the same size applied for all four corners.
 * @param size Size in [Dp] to apply.
 */
fun AnimatableRoundedCornerShape(size: Dp) =
    AnimatableRoundedCornerShape(CornerSize(size))

/**
 * Creates [AnimatableRoundedCornerShape] with the same size applied for all four corners.
 * @param size Size in pixels to apply.
 */
fun AnimatableRoundedCornerShape(size: Float) =
    AnimatableRoundedCornerShape(CornerSize(size))

/**
 * Creates [AnimatableRoundedCornerShape] with the same size applied for all four corners.
 * @param percent Size in percents to apply.
 */
fun AnimatableRoundedCornerShape(percent: Int) =
    AnimatableRoundedCornerShape(CornerSize(percent))

/**
 * Creates [AnimatableRoundedCornerShape] with sizes defined in [Dp].
 */
fun AnimatableRoundedCornerShape(
    topStart: Dp = 0.dp,
    topEnd: Dp = 0.dp,
    bottomEnd: Dp = 0.dp,
    bottomStart: Dp = 0.dp
) = AnimatableRoundedCornerShape(
    topStart = CornerSize(topStart),
    topEnd = CornerSize(topEnd),
    bottomEnd = CornerSize(bottomEnd),
    bottomStart = CornerSize(bottomStart)
)

/**
 * Creates [AnimatableRoundedCornerShape] with sizes defined in pixels.
 */
fun AnimatableRoundedCornerShape(
    topStart: Float = 0.0f,
    topEnd: Float = 0.0f,
    bottomEnd: Float = 0.0f,
    bottomStart: Float = 0.0f
) = AnimatableRoundedCornerShape(
    topStart = CornerSize(topStart),
    topEnd = CornerSize(topEnd),
    bottomEnd = CornerSize(bottomEnd),
    bottomStart = CornerSize(bottomStart)
)

/**
 * Creates [AnimatableRoundedCornerShape] with sizes defined in percents of the shape's smaller side.
 *
 * @param topStartPercent The top start corner radius as a percentage of the smaller side, with a
 * range of 0 - 100.
 * @param topEndPercent The top end corner radius as a percentage of the smaller side, with a
 * range of 0 - 100.
 * @param bottomEndPercent The bottom end corner radius as a percentage of the smaller side,
 * with a range of 0 - 100.
 * @param bottomStartPercent The bottom start corner radius as a percentage of the smaller side,
 * with a range of 0 - 100.
 */
fun AnimatableRoundedCornerShape(
    /*@IntRange(from = 0, to = 100)*/
    topStartPercent: Int = 0,
    /*@IntRange(from = 0, to = 100)*/
    topEndPercent: Int = 0,
    /*@IntRange(from = 0, to = 100)*/
    bottomEndPercent: Int = 0,
    /*@IntRange(from = 0, to = 100)*/
    bottomStartPercent: Int = 0
) = AnimatableRoundedCornerShape(
    topStart = CornerSize(topStartPercent),
    topEnd = CornerSize(topEndPercent),
    bottomEnd = CornerSize(bottomEndPercent),
    bottomStart = CornerSize(bottomStartPercent)
)
