package com.colorata.shapify

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.InfiniteRepeatableSpec
import androidx.compose.animation.core.InfiniteTransition
import androidx.compose.animation.core.TargetBasedAnimation
import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.Stable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.withFrameNanos

@Stable
private val defaultAnimation = tween<Float>()

@Composable
fun animateShapeAsState(
    targetValue: AnimatableShape,
    rotateInDegrees: Float = 0f,
    rotateOutDegrees: Float = 0f,
    animationSpec: TweenSpec<Float> = defaultAnimation
): State<AnimatableShape> {
    val shape = remember { mutableStateOf(targetValue) }
    val animation = remember(animationSpec) {
        TargetBasedAnimation(
            animationSpec = animationSpec,
            typeConverter = Float.VectorConverter,
            initialValue = 0f,
            targetValue = 2f
        )
    }
    var playTime by remember { mutableStateOf(0L) }
    LaunchedEffect(targetValue) {
        if (shape.value == targetValue) return@LaunchedEffect
        val initial = shape.value
        val target = targetValue

        val startTime = withFrameNanos { it }
        do {
            playTime = withFrameNanos { it } - startTime
            val currentValue = animation.getValueFromNanos(playTime)
            if (currentValue <= 1f) {
                shape.value = shape.value.copy(
                    fractionToCircle = currentValue,
                    rotation = ((initial.rotation.degrees)..(-rotateOutDegrees - initial.rotation.degrees) fraction currentValue).degrees
                )
            } else {
                shape.value = targetValue.copy(
                    fractionToCircle = 2f - currentValue,
                    rotation = ((target.rotation.degrees)..(rotateInDegrees + target.rotation.degrees) fraction (2f - currentValue)).degrees
                )
            }
        } while (!animation.isFinishedFromNanos(playTime))

        playTime = 0L
    }
    return shape
}

@Composable
fun AnimatableShape.animateFractionToCircleAsState(
    fraction: Float,
    animationSpec: TweenSpec<Float> = defaultAnimation
): State<AnimatableShape> {
    val shape = remember { mutableStateOf(this) }
    val updatedFraction by animateFloatAsState(fraction, animationSpec)
    LaunchedEffect(updatedFraction, this) {
        shape.value = copy(fractionToCircle = updatedFraction)
    }
    return shape
}

@Composable
fun AnimatableShape.animateRotationAsState(
    rotationDegrees: Float,
    animationSpec: AnimationSpec<Float> = defaultAnimation
): State<AnimatableShape> {
    val shape = remember { mutableStateOf(this) }
    val updatedRotation by animateFloatAsState(rotationDegrees, animationSpec)
    LaunchedEffect(updatedRotation, this) {
        shape.value = copy(rotation = updatedRotation.degrees)
    }
    return shape
}

@Composable
fun InfiniteTransition.animateShapeFractionToCircle(
    shape: AnimatableShape,
    initialFraction: Float,
    targetFraction: Float,
    animationSpec: InfiniteRepeatableSpec<Float>
): State<AnimatableShape> {
    val fractionToCircle by animateFloat(initialFraction, targetFraction, animationSpec)
    val updatedShape = remember(fractionToCircle, shape) {
        mutableStateOf(
            shape.copy(
                fractionToCircle =
                fractionToCircle
            )
        )
    }
    return updatedShape
}

@Composable
fun InfiniteTransition.animateShapeRotation(
    shape: AnimatableShape,
    initialRotationDegrees: Float,
    targetRotationDegrees: Float,
    animationSpec: InfiniteRepeatableSpec<Float>
): State<AnimatableShape> {
    val rotation by animateFloat(initialRotationDegrees, targetRotationDegrees, animationSpec)
    val updatedShape = remember(rotation, shape) {
        mutableStateOf(
            shape.copy(
                rotation =
                rotation.degrees
            )
        )
    }
    return updatedShape
}
