package com.colorata.shapify

fun PolygonShape(sides: Int) = SinusoidalShape(
    waveHeight = 1f / (sides.toFloat() * sides.toFloat() + 1f),
    periodMultiplier = sides.toFloat()
)
