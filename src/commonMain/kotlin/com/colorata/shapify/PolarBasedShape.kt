package com.colorata.shapify

import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin

abstract class PolarBasedShape(
    private val laps: Float = 1f
) : AnimatableShape() {

    abstract val polarFunction: (theta: Float) -> Float?

    private val radiusFromTheta: HashMap<Float, Float?> = hashMapOf()

    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        val step = (density.density / minOf(size.width, size.height))
        radiusFromTheta.putAll(
            polarBasedHashmap(
                step = step,
                laps = laps,
                function = polarFunction
            )
        )

        val maxDeviation = radiusFromTheta.maxOf {
            val v = it.value ?: Float.MIN_VALUE
            v / maxAllowedRadiusFromRadians(it.key)
        }

        radiusFromTheta.forEach { (key, value) ->
            if (value != null) radiusFromTheta[key] = value / maxDeviation
        }

        return Outline.Generic(
            createPolarBasedPath(
                size, radiusFromTheta
            )
        )
    }
}

private fun polarBasedHashmap(
    step: Float,
    laps: Float = 1f,
    function: (theta: Float) -> Float?
): HashMap<Float, Float?> {
    val radiusFromTheta = hashMapOf<Float, Float?>()

    var angle = 0f
    while (angle <= laps * pi * 2 + step) {
        val wp = function(angle)
        radiusFromTheta[angle] = wp
        angle += step
    }
    return radiusFromTheta
}

private fun createPolarBasedPath(
    size: Size,
    radiusFromTheta: HashMap<Float, Float?>
): Path {
    val stretch = false
    val min = min(size.width, size.height)
    val w = if (stretch) size.width else min
    val h = if (stretch) size.height else min
    val xOffset = if (stretch || min == size.width) 0f else (size.width - size.height) / 2
    val yOffset = if (stretch || min == size.height) 0f else (size.height - size.width) / 2



    return Path().apply {
        var moveDone = false
        val startAngle = 0f
        val start = radiusFromTheta[startAngle]?.let {
            polarCoordinates(it, startAngle.radians)
                .rectangularCoordinates
        }
        if (start != null) {
            moveTo(start.x * w / 2 + w / 2, start.y * h / 2 + h / 2)
            moveDone = true
        }
        radiusFromTheta.toSortedMap().forEach { (angle, radius) ->
            val wp = radius?.let {
                polarCoordinates(it, angle.radians)
                    .rectangularCoordinates
            }
            if (wp != null) {
                if (!moveDone) {
                    moveTo(wp.x * w / 2 + w / 2 + xOffset, wp.y * h / 2 + h / 2 + yOffset)
                    moveDone = true
                }
                lineTo(wp.x * w / 2 + w / 2 + xOffset, wp.y * h / 2 + h / 2 + yOffset)
            } else {
                moveDone = false
            }
        }
    }
}

fun maxAllowedRadiusFromRadians(radians: Float): Float {
    return 1 / maxOf(abs(cos(radians)), abs(sin(radians)))
}
