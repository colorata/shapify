package com.colorata.shapify

import androidx.compose.runtime.Stable
import kotlin.math.PI

internal const val pi = PI.toFloat()

/**
 * Angle class to keep angles consistent and not confusing
 * @param raw value of angle. Should not be used directly
 */
@Stable
sealed class Angle(val raw: Float) {
    /**
     * Angle in radians.
     * @param value value of angle in degrees
     */
    class Radians(val value: Float) : Angle(value) {

        override fun hashCode(): Int {
            return value.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Radians

            if (value != other.value) return false

            return true
        }
    }

    /**
     * Angle in degrees.
     * @param value value of angle in degrees
     */
    class Degrees(val value: Float) : Angle(value) {


        override fun hashCode(): Int {
            return value.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Degrees

            if (value != other.value) return false

            return true
        }
    }

    override fun hashCode(): Int {
        return raw.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Angle

        if (degrees != other.degrees) return false

        return true
    }

    companion object
}

operator fun Angle.plus(other: Angle): Angle {
    return degrees(this.degrees + other.degrees)
}

operator fun Angle.unaryMinus(): Angle {
    return degrees(-this.degrees)
}

operator fun Angle.div(other: Int): Angle {
    return degrees(this.degrees / other.toFloat())
}

/**
 * Converts current angle to degrees. Can be used only for radians
 */
private fun Angle.Radians.toDegrees() = Angle.Degrees(value * 180f / pi)

/**
 * Converts current angle to radians. Can be used only for degrees
 */
private fun Angle.Degrees.toRadians() = Angle.Radians(value * pi / 180f)

/**
 * Auto converted angle in radians
 */
val Angle.radians: Float
    get() {
        return when (this) {
            is Angle.Radians -> this.value
            is Angle.Degrees -> this.toRadians().value
        }
    }

/**
 * Auto converted angle in degrees
 */
val Angle.degrees: Float
    get() {
        return when (this) {
            is Angle.Radians -> this.toDegrees().value
            is Angle.Degrees -> this.value
        }
    }

private fun degrees(value: Float) = Angle.Degrees(value)

private fun radians(value: Float) = Angle.Radians(value)


val Float.degrees: Angle
    get() = degrees(this)

val Float.radians: Angle
    get() = radians(this)

