@file:Suppress("unused")

package com.colorata.shapify

import kotlin.math.*

/**
 * Class to create Coordinate in specific coordinates system
 * Available systems:
 * - Rectangular
 * - Polar
 */
sealed class Coordinates {
    /**
     * Creates rectangular coordinate in Rectangular system
     * @param x X coordinate(horizontal axis)
     * @param y Y coordinate(vertical axis)
     */
    data class Rectangular(
        val x: Float, val y: Float
    ) : Coordinates()

    /**
     * Creates polar coordinate in Polar System
     * @param radius Radius of coordinate
     * @param theta Theta(angle) of coordinate in radians
     */
    data class Polar(
        val radius: Float, val theta: Angle
    ) : Coordinates()
}

/**
 * Creates polar coordinate in Polar System
 * @param radius Radius of coordinate
 * @param theta Theta(angle) of coordinate in radians
 */
fun polarCoordinates(radius: Float, theta: Angle) = Coordinates.Polar(radius, theta)

/**
 * Creates rectangular coordinate in Rectangular system
 * @param x X coordinate(horizontal axis)
 * @param y Y coordinate(vertical axis)
 */
fun rectangularCoordinates(x: Float, y: Float) = Coordinates.Rectangular(x, y)

/**
 * Converts [Coordinates.Polar] to [Coordinates.Rectangular]
 * @return [Coordinates.Rectangular]
 */
private fun Coordinates.Polar.toRectangular() = rectangularCoordinates(
    x = radius * cos(theta.radians), y = radius * sin(theta.radians)
)

/**
 * Converts [Coordinates.Rectangular] coordinates to [Coordinates.Polar]
 */
private fun Coordinates.Rectangular.toPolar() =
    polarCoordinates(sqrt(x.pow(2) + y.pow(2)), atan2(y, x).radians)

/**
 * Returns polar coordinates of [Coordinates]
 */
val Coordinates.polarCoordinates: Coordinates.Polar
    get() {
        return when (this) {
            is Coordinates.Polar -> this
            is Coordinates.Rectangular -> toPolar()
        }
    }

/**
 * Returns rectangular coordinates of [Coordinates]
 */
val Coordinates.rectangularCoordinates: Coordinates.Rectangular
    get() {
        return when (this) {
            is Coordinates.Polar -> toRectangular()
            is Coordinates.Rectangular -> this
        }
    }
